<?php

namespace Drupal\thinkshout_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ThinkShoutForm extends FormBase {

  public function getFormId() {
    return 'thinkshout_form_adder';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['first'] = [
      '#type' => 'number',
      '#title' => $this->t('First Number'),
      '#required' => TRUE,
    ];
    $form['second'] = [
      '#type' => 'number',
      '#title' => $this->t('Second Number'),
      '#required' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $first = $form_state->getValue('first');
    $second = $form_state->getValue('second');
    drupal_set_message($this->t('The sum is %sum.', ['%sum' => $first + $second]));
  }
}
